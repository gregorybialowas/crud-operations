<?php
class postActions {

    /**        add / insert new rows        **/
    function insert2db($f_table, $f_records)
    {
        $f_count = 0;
        foreach ($f_records as $key => $val)
        {
            if ($f_count == 0) {
              $f_fields = $key;
              if ($val == '') $f_values = "''"; else $f_values = "'" . $val . "'";
            }
            else {
              $f_fields .= ", " . $key;
              if ($val == '') $f_values .= ", ''"; else $f_values .= ", '" . $val . "'";
            }
        $f_count++;
        }
        $query = mysql_query("INSERT INTO " . $f_table . " (" . $f_fields . ") VALUES (" . $f_values . ")") or die (mysql_error());
    }


    /**        select from table        **/
    function selectfromdb ($f_what, $f_table, $f_condition='', $f_ekstra='')
    {
      if ($f_condition != '') $f_condition = ' where ' . $f_condition;
      if ($f_ekstra != '') $f_ekstra = ' ' . $f_ekstra;

      $query = mysql_query("SELECT " . $f_what . " FROM " . $f_table . $f_condition . $f_ekstra . "") or die (mysql_error());
      return $query;
    }

    /**        update table        **/
    function updatedb($f_table, $f_records, $f_condition)
    {
        $f_count = 0;
        foreach ($f_records as $key => $val)
        {
            if ($f_count == 0) {
            $f_fields = $key . " = '" . $val . "'";
        }
            else {
            $f_fields .= ", " . $key . " = '" . $val . "'";
        }

        $f_count++;
        }
        $query = mysql_query("UPDATE " . $f_table . " SET " . $f_fields . " where " . $f_condition . "") or die(mysql_error());
    }


    /**        delete rows        **/
    function deletefromdb($f_table, $f_condition)
    {
      $query = mysql_query("DELETE from " . $f_table . " where " . $f_condition . "") or die (mysql_error());
    }
}
