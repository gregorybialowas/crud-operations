Very small class I have written to speed up basic Database operations. This works just fine with pretty simple db manipulations like SELECT, INSERT, UPDATE and DELETE, also known as CRUD operations.

CRUD stands for __C__reate __R__ead __U__pdate __D__elete.

##Usage

First we need to initialize the class:

	<?php
	$action = new postActions();
	/*  ***   $action can be replaced with any name of your liking   ***  */
	?>

For the demonstration purposes let's assume there's a DB with a table Profile. Profile has only three columns: ID, FirstName and LastName.

###INSERT demo

	<?php
		$insert_array = array(
		 'FirstName' => $_POST['FirstName'],
		 'LastName' => $_POST['LastName'],
		);
		$action->insert2db('Profile', $insert_array);
	?>

###SELECT demo

Select all:

	<?php
	$q = $action->selectfromdb('*', 'Profile');
	?>

Select all and list them in descending order by last name:

	<?php
	$q = $action->selectfromdb('*', 'Profile', '', 'ORDER BY LastName DESC');
	?>

Select only a particular ID:

	<?php
	$q = $action->selectfromdb('*', 'Profile', 'id = \'4\'');
	?>
	
Once we've selected what we wanted, the rest proceeds as a regular PHP / MySQL syntax:

	<?php
	while($row = mysql_fetch_array($q)) {
	..
	..
	..
	}
	?>

###UPDATE demo

	<?php
		$update_array = array(
		 'FirstName' => $_POST['FirstName'],
		 'LastName' => $_POST['LastName'],
		);
		$action->updatedb('Profile', $update_array, 'id = \'' . $_GET['inputFieldsValue'] . '\'');
	?>

###DELETE demo

	<?php
	$action->deletefromdb('Profile', 'id = \'' . $_GET['inputFieldsValue'] . '\'');
	?>

And that's it!

##[ For more info go here ](http://gregbialowas.com/db-operations)